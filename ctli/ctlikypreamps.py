# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Emilio Morales"
__copyright__ = "Copyright 2021, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

import sys
import os

import traceback

from taurus.external.qt import Qt
from taurus.external.qt.QtGui import QMessageBox

try:
    import tango as pt
except Exception as e:
    print("Importing PyTango as pt: {}".format(e))
    import PyTango as pt

from .widgets.klystronpreamp import LinacPreAmp

CLASS_NAME = "LinacData"
INSTALLED_ATT_NAME = "KA_PreAmp_Installed_active"
SN_ATTR_NAME = "KA_PreAmp_SN_active"


class PreAmps(Qt.QWidget):

    def __init__(self, class_name, attribute_name, parent=None):
        super(Qt.QWidget, self).__init__()

        self.class_name = class_name
        self.attribute_name = attribute_name

        self.devices = [a for a in self.get_devices() if "21" not in a]
        print(self.devices)

        self.layout = Qt.QHBoxLayout(self)
        self.add_widgets()

    def add_widgets(self):

        try:
            widget = Qt.QWidget()
            layout = Qt.QHBoxLayout(widget)
            layout.addStretch()

            for device in self.devices:
                layout.addWidget(LinacPreAmp(device))

            self.layout.addWidget(widget)
            
            self.setFixedWidth(widget.sizeHint().width() +
                               25 * (len(self.devices)))
            self.setFixedHeight(widget.sizeHint().height() +
                                25 * (len(self.devices)))

        except Exception as e:
            print("Problems to generate KY widgets: {}".format(e))
            traceback.print_exc()

    def get_devices(self):

        try:
            db = pt.Database()
            if self.class_name in db.get_class_list(self.class_name):
                devices = list(db.get_device_name('*', self.class_name))
                devs = []
                for item in devices:
                    if not "21" in item:
                        aux = pt.DeviceProxy(item)
                        if self.attribute_name in aux.get_attribute_list():
                            devs.append(item)
            if len(devs) > 0:
                return devs
            else:
                error_dialog = QMessageBox()
                error_dialog.setIcon(QMessageBox.Critical)
                error_dialog.setText(
	                "There is not {} class with {} attribute in this Tango DB. "
	                "Please check.".format(self.class_name, self.attribute_name)
                )
                sys.exit(error_dialog.exec_())

        except Exception as e:
            print("Problems at PreAmps.get_devices: {}".format(e))
            traceback.print_exc()


def main():
    app = Qt.QApplication(sys.argv)
    GUI = PreAmps(CLASS_NAME, INSTALLED_ATT_NAME)
    GUI.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
