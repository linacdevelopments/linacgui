import os
import sys

from taurus.external.qt import Qt, QtCore, QtWidgets, uic
from taurus.core import TaurusEventType
from taurus.qt.qtgui.display import TaurusLabel, TaurusLed
from taurus.qt.qtgui.input import TaurusValueCheckBox

import traceback
import time

try:
	import tango
except Exception as e:
	print("Importing PyTango as tango: {}".format(e))
	import PyTango as tango

PROC_DEV = "li/ct/rf"


class MyTaurusValueCheckBox(TaurusValueCheckBox):

	def __init__(self, *args, **kwargs):
		super(MyTaurusValueCheckBox, self).__init__()

	def handleEvent(self, evt_src, evt_type, evt_value):
		super(MyTaurusValueCheckBox, self).handleEvent(evt_src, evt_type,
		                                               evt_value)
		if self.getValue() and not evt_value.rvalue:
			self.setValue(False)


class MyTaurusLed(TaurusLed):

	def __init__(self, *args, **kwargs):

		super(MyTaurusLed, self).__init__()

	def handleEvent(self, evt_src, evt_type, evt_value):
		if evt_type in [TaurusEventType.Change]:
			super(MyTaurusLed, self).handleEvent(evt_src, evt_type, evt_value)
			self.updateWidget(evt_value)

	def updateWidget(self, evt_value):

		if evt_value.rvalue in [5]:
			self.setLedColor("green")  # Set color to Green

		elif evt_value.rvalue in [0]:
			self.setLedColor("black")  # Set color to Black

		else:
			self.setLedColor("orange")  # Set color to Orange


class MyTaurusSNLabel(TaurusLabel):

	def __init__(self, rfout_widget, rfout_widget_v):
		super(MyTaurusSNLabel, self).__init__()

		self.rfout_widget = rfout_widget
		self.rfout_widget_v = rfout_widget_v
	def handleEvent(self, evt_src, evt_type, evt_value):

		if evt_type in [TaurusEventType.Change]:
			super(MyTaurusSNLabel, self).handleEvent(evt_src, evt_type,
			                                       evt_value)

			self.updateWidget(evt_src)

			dev = str(evt_src).rstrip('_numeric') + "_meaning"
			self.setText(tango.AttributeProxy(dev).read().value)

	def updateWidget(self, evt_src):

		try:
			dev = str(evt_src).rstrip('_numeric') + "_active"
			sn = tango.AttributeProxy(dev).read().value

			if "plc4" in str(evt_src).lower():
				KA_name = "KA1"
				attr_v = 'li/RF/tti/TTI_KY1_Mean'
			elif "plc5" in str(evt_src).lower():
				KA_name = "KA2"
				attr_v = 'li/RF/tti/TTI_KY2_Mean'

			attr = "{}/{}_RF_out_TTI_{}_Watt".format(PROC_DEV, KA_name, sn)
			self.rfout_widget.setModel(attr)
			self.rfout_widget_v.setModel(attr_v)

		except Exception as e:
			print("Problems in uptdateRFoutTTI: {}".format(e))
			print(traceback.format_exc())


class MyTaurusLabel(TaurusLabel):

	_sn_attr = "KA_PreAmp_SN"

	def __init__(self, widget_list, gain_widget_list, rfout_widget, sn_widget,
	             rfout_widget_v, *args, **kwargs):

		super(MyTaurusLabel, self).__init__()

		self.widget_list = widget_list
		self.gain_widget_list = gain_widget_list
		self.rfout_widget = rfout_widget
		self.rfout_widget_v = rfout_widget_v
		self.sn_widget = sn_widget

	def handleEvent(self, evt_src, evt_type, evt_value):

		if evt_type in [TaurusEventType.Change]:
			super(MyTaurusLabel, self).handleEvent(evt_src, evt_type,
			                                       evt_value)
			self.updateWidgets(evt_value)
			self.updateModels(evt_value, evt_src)

			dev = str(evt_src).rstrip('_numeric') + "_meaning"
			self.setText(tango.AttributeProxy(dev).read().value)

	def updateWidgets(self, evt_value):

		for item in self.widget_list:
			if evt_value.rvalue in [1]:
				item.setEnabled(True)
			elif evt_value.rvalue in [3]:
				if "tlabel_ka_sn_write" in str(item.objectName()):
					item.setEnabled(True)
				else:
					item.setEnabled(False)
			elif evt_value.rvalue in [2,4]:
				item.setEnabled(False)


		for item in self.gain_widget_list:
			if evt_value.rvalue in [1,2]:
				item.setEnabled(True)
			else:
				item.setEnabled(False)

	def uptdateRFoutTTI(self, plc):
		KA_name = None
		dev = None

		try:
			if plc in ["plc4"]:
				KA_name = "KA1"
				attr_v = 'li/RF/tti/TTI_KY1_Mean'
				dev = "li/ct/plc4"
			elif plc in ["plc5"]:
				KA_name = "KA2"
				attr_v = 'li/RF/tti/TTI_KY2_Mean'
				dev = "li/ct/plc5"

			sn_attr = "{}/{}_active".format(dev, self._sn_attr)
			sn = tango.AttributeProxy(sn_attr).read().value

			if sn and KA_name:
				attr = "{}/{}_RF_out_TTI_{}_Watt".format(PROC_DEV, KA_name, sn)
				self.rfout_widget.setModel(attr)
				self.rfout_widget_v.setModel(attr_v)
			else:
				raise ValueError("No SN as been set for this TTI amplifier")

		except Exception as e:
			print("Problems in uptdateRFoutTTI: {}".format(e))
			print(traceback.format_exc())

	def updateModels(self, evt_value, evt_src):

		if "plc" in str(evt_src).lower():
			if "plc4" in str(evt_src).lower():
				plc = "plc4"
				model_eads = "sr04/vc/eps-plc-01/li_ct_ifbo_rka03a05_01_rfout_af"
			elif "plc5" in str(evt_src).lower():
				plc = "plc5"
				model_eads = "sr04/vc/eps-plc-01/li_ct_ifbo_rka03a08_01_rfout_af"
		elif "li-01" in str(evt_src).lower():
			plc = "lab"
			model_eads = "sys/tg_test/1/long_scalar"

		if evt_value.rvalue in [1, 3]:
			self.uptdateRFoutTTI(plc)
		elif evt_value.rvalue in [2, 4]:
			self.rfout_widget.setModel(model_eads)


class LinacPreAmp(QtWidgets.QWidget):
	_KA_reset = '/TTI_intlk_res'

	def __init__(self, dev_name, parent=None):

		super(LinacPreAmp, self).__init__()

		self.dev_name = dev_name

		uipath = os.path.join(os.path.dirname(__file__), "ui",
		                      "LinacPreAmp.ui")
		self.ui = uic.loadUi(uipath, self)

		# GUI title:
		self.ui.setWindowTitle('Linac KA preamplifier GUI')

		self.change_title_label()

		self.cb_reset = MyTaurusValueCheckBox()
		self.tlabel_ka_sn_read = MyTaurusSNLabel(self.tlabel_ka_rfout_read,
		                                         self.tlabel_ka_rfout_read_v)

		self.gui_obj = {
			"KA": [self.tled_ka_on, self.tvcheck_ka_on, self.cb_reset,
			       self.tled_reset, self.tlabel_ka_status_read,
			       self.tlabel_ka_sn_write],
			"KA_gain": [self.tlabel_ka_gain_write,
			            self.tlabel_ka_gain_read],
			"KA_rfout": self.tlabel_ka_rfout_read,
			"KA_SN": self.tlabel_ka_sn_read,
			"KA_rfout_v": self.tlabel_ka_rfout_read_v,
		}

		self.tlabel_ka_type_read = MyTaurusLabel(self.gui_obj["KA"],
		                                         self.gui_obj["KA_gain"],
		                                         self.gui_obj["KA_rfout"],
		                                         self.gui_obj["KA_SN"],
		                                         self.gui_obj["KA_rfout_v"])
		self.tled_ka_status = MyTaurusLed()

		self.cb_values = self.get_options("KA_PreAmp_Installed_options")
		self.cb_sn_values = self.get_options("KA_PreAmp_SN_options")

		self.insertTaurusValueCheckBox()
		self.insertCustomWidgets()

		self.setTaurusModels()

	def get_options(self, op_attr):

		attr = tango.AttributeProxy("{}/{}".format(self.dev_name, op_attr))

		preamps = attr.read().value.split(', ')

		aux_list = []

		for item in preamps:
			aux_list.append((item.replace('_', ' '), item))

		return tuple(aux_list)

	def change_title_label(self):

		if "plc4" in self.dev_name.lower():
			self.label_9.setText("Pre-Amp. KA 1")
		elif "plc5" in self.dev_name.lower():
			self.label_9.setText("Pre-Amp. KA 2")
		else:
			self.label_9.setText("TEST AT L-21")

	def insertTaurusValueCheckBox(self):

		self.cb_reset.setAutoApply(True)
		self.cb_reset.setShowText(False)
		self.gridLayout_2.addWidget(self.cb_reset, 4, 2)

	def insertCustomWidgets(self):
		"""
		Method that insert customized taurus widgets in GUI layout.
		:return:
		"""
		self.tlabel_ka_sn_read.setFixedWidth(120)
		self.tlabel_ka_sn_read.setFixedHeight(30)
		self.gridLayout_2.addWidget(self.tlabel_ka_sn_read, 1, 2)

		self.tlabel_ka_type_read.setFixedWidth(120)
		self.tlabel_ka_type_read.setFixedHeight(30)
		self.gridLayout_2.addWidget(self.tlabel_ka_type_read, 0, 2)

		self.tled_ka_status.setFixedWidth(24)
		self.tled_ka_status.setFixedHeight(24)
		self.tled_ka_status.setAlignment(QtCore.Qt.AlignRight)
		self.gridLayout_2.addWidget(self.tled_ka_status, 3, 0)
		self.gridLayout_2.setAlignment(self.tled_ka_status,
		                               QtCore.Qt.AlignRight)

	def setTaurusModels(self):
		"""
		Method to set Taurus models in every widget.
		:return:
		"""

		try:
			self.cb_reset.setModel(self.dev_name + self._KA_reset)
			self.tled_reset.setModel(self.dev_name + self._KA_reset)
			self.tled_ka_on.setModel(self.dev_name + '/TTI_ONC')
			self.tvcheck_ka_on.setModel(self.dev_name + '/TTI_ONC')

			self.tlabel_ka_type_write.setModel(self.dev_name +
			                                   '/KA_PreAmp_Installed_active')
			self.tlabel_ka_type_write.addValueNames(self.cb_values)

			self.tlabel_ka_sn_write.setModel(self.dev_name +
			                                 '/KA_PreAmp_SN_active')
			self.tlabel_ka_sn_write.addValueNames(self.cb_sn_values)
			self.tlabel_ka_type_read.setModel(self.dev_name +
			                                  '/KA_PreAmp_Installed_numeric')
			self.tlabel_ka_sn_read.setModel(self.dev_name +
			                                '/KA_PreAmp_SN_numeric')

			self.tlabel_ka_status_read.setModel(
				self.dev_name + '/TTI_st_Status')
			self.tled_ka_status.setModel(self.dev_name + '/TTI_st')

			self.tlabel_ka_gain_write.setModel(self.dev_name +
			                                   '/Amp_gain_setpoint')
			self.tlabel_ka_gain_read.setModel(self.dev_name + '/Amp_gain')

		except Exception as e:
			print("Problems in setTaurusModels: {}".format(e))
