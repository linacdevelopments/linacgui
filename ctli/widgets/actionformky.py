# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Enmilio Morales"
__copyright__ = "Copyright 2024, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

import os

import tango
from taurus.external.qt import Qt
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.util.ui import UILoadable
import traceback

@UILoadable(with_ui="_ui")
class ActionFormKY(TaurusWidget):
    def __init__(self, parent=None, name=None, designMode=False):
        try:
            self.__name = name.__name__
        except Exception:
            self.__name = "ActionFormKY"
        super(ActionFormKY, self).__init__(parent, designMode=designMode)
        try:
            self.debug("[{}]__init__()".format(self.__name))
            basePath = os.path.dirname(__file__)
            if len(basePath) == 0:
                basePath = '.'
            self.loadUi(filename="actionWidgetKY.ui",
                        path=basePath+"/ui")

        except Exception as e:
            self.warning(
                "[{}]__init__(): Widget exception! {}".format(self.__name, e)
            )
            traceback.print_exc()
            self.traceback()

    def getAttributeRamp(self, cmd: str):
        self.attributeRamp = cmd

    def getAttributeModel(self, attr):
        self.modelHVPS_ONC = attr

    def getRampModel(self, rmp: str):
        self.rampBool = rmp

    def setupAction(self):
        _sender = self.sender()
        _modelHVPS_ONC = self._ui.Led.model

        if "plc4" in _modelHVPS_ONC:
            _rampBool = "li/ct/ramp-01/RampKY1"
            _cmd = "li/ct/plc4/hvps_v_setpoint"
        elif "plc5" in _modelHVPS_ONC:
            _rampBool = "li/ct/ramp-01/RampKY2"
            _cmd = "li/ct/plc5/hvps_v_setpoint"

        attr_value = tango.AttributeProxy(_modelHVPS_ONC).read().value

        try:
            ramp_value = tango.AttributeProxy(_rampBool).read().value
        except Exception:
            ramp_value = False

        if _sender.isChecked() is True:
            if ramp_value is True:
                if attr_value is False:
                    tango.DeviceProxy("li/ct/ramp-01").command_inout(
                        "ramp_attribute", cmd_param=_cmd
                    )
            else:
                tango.AttributeProxy(_modelHVPS_ONC).write(True)
        else:
            tango.AttributeProxy(_modelHVPS_ONC).write(False)

    @classmethod
    def getQtDesignerPluginInfo(cls):
        ret = TaurusWidget.getQtDesignerPluginInfo()
        ret['module'] = 'actionformky'
        ret['group'] = 'Taurus Linac Widgets'
        ret['container'] = ':/designer/dialogbuttonbox.png'
        ret['container'] = False
        return ret


def main():
    app = Qt.QApplication(sys.argv)
    w = ActionFormKY()
    w.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
